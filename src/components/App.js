import React, { Component } from 'react';
import NavColumn from './NavColumn'
import { Switch, Route } from 'react-router-dom'
import Header from './Header'
import CreateOrganization from './organization/CreateOrganization'
import OrganizationGrid from './organization/OrganizationGrid'
import Login from './Login'
import '../styles/NavColumn.css';
import { AUTH_TOKEN } from 'constants'

class App extends Component {
  render() {
    const authToken = localStorage.getItem(AUTH_TOKEN)
    return (
      authToken ?
      <div className="container-fluid">
        <Header/>
        <div className="row">
          <nav className="col-sm-3 col-md-2 hidden-xs-down bg-faded sidebar NavColumn">
              <NavColumn />
          </nav>
        <main className="col-sm-9 col-md-10 pt-3">
         <section className="row text-center placeholders">
            <Switch>
              <Route exact path="/createOrganization" component={CreateOrganization}/>
              <Route exact path="/OrganizationGrid" component={OrganizationGrid} />
            </Switch>
          </section>
        </main>
      </div>
    </div>
    :
    <Login/>
    );
  }
}

export default App;
