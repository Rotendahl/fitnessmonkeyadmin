import React, { Component } from 'react'
import '../styles/Header.css';

class Header extends Component{
  render(){
    return(
      <div className="row Header">
        <div className="col-2 Header" style={{backgroundColor: "#5e605f", padding: "10px 20px"}}>
          <img className="rounded-circle" src="http://via.placeholder.com/70x70"/>
        </div>
        <div className="col-10 Header" style={{backgroundColor: "#4c4e4d"}}></div>
      </div>
    )
  }
}

export default Header
