import React, {Component} from 'react'
import gql from 'graphql-tag'
import { graphql } from 'react-apollo'

import {AUTH_TOKEN} from './constants'
import "../styles/login.css"

class Login extends Component {
  state = {
      email: '',
      password: '',
      failed : false
  }
_login = async(e) => {
  e.preventDefault()
  const {email, password} = this.state
  const result = await this.props.login({
    variables: {
      input: {
        email: email,
        password: password
      }
    }
  })
  const { successful }  = result.data.login
  if (successful){
    const {token} = result.data.login.payload
    localStorage.setItem(AUTH_TOKEN, token)
    this.props.history.pushState(null, '/');
  }
  else {
    this.setState({
      email: '',
      password: '',
      failed: true
    })
  }
}

render() {
return (
  <div className="container">
    <div className="text-center" style={{padding: "70px 20px"}}>
      <img className="rounded-circle" src="http://via.placeholder.com/300x300"/>
    </div>

    <form className="form-group" onSubmit={(e) => this._login(e)}>
    <div className="row">
      <div className="col-md-3"></div>
        <div className="col-md-6 text-center">
          <h2>FitnessMonkey Login</h2>
          <hr/>
        </div>
    </div>
    <div className="row">
      <div className="col-md-3"></div>
        <div className="col-md-6">
          <div className="form-group has-danger">
            <label className="sr-only" htmlFor="email">E-Mail Address</label>
              <div className="input-group mb-2 mr-sm-2 mb-sm-0">
                <div className="input-group-addon" style={{width: "2.6rem"}}>
                  <i className="fa fa-at"></i>
                </div>
                <input
                  type="text"
                  name="email"
                  className="form-control"
                  id="email"
                  placeholder="din@mail.dk"
                  required="required"
                  autoFocus="autofocus"
                  value={this.state.email}
                  onChange={e => this.setState({ email: e.target.value })}
                />
              </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-md-3"></div>
          <div className="col-md-6">
            <div className="form-group">
              <label className="sr-only" htmlFor="password">Password</label>
              <div className="input-group mb-2 mr-sm-2 mb-sm-0">
                <div className="input-group-addon" style={{width: "2.6rem"}}>
                  <i className="fa fa-key"></i>
                </div>
                <input
                  type="password"
                  name="password"
                  className="form-control"
                  id="password"
                  placeholder="Password"
                  required="required"
                  value={this.state.password}
                  onChange={e => this.setState({ password: e.target.value })}
                />

              </div>
            </div>
          </div>
        </div>
        <div className="mx-auto" style={{paddingTop: "1rem", width:"100px"}}>
              <button
                type="submit" className=" mx-auto btn btn-success">
                <i className="fa fa-sign-in"></i>
                Login
              </button>
        </div>
        {this.state.failed &&
          <div className="mx-auto bounce-in-bottom" style={{paddingTop: "1rem", width: "40%"}}>
                <div
                  className="alert alert-danger" role="alert">
                  Forkert email eller adgangskode
                </div>
          </div>
        }
    </form>
    </div>
  )}
}

const LOGIN_MUTATION = gql`
mutation Login($input: LoginInput!) {
  login(input: $input) {
    successful
    payload {
      user {
        id
        fbId
        name
				email
      }
			token
    }
    errors {
      type
      metadata {
        key
        value
      }
      code
    }
  }
}
`

export default graphql(LOGIN_MUTATION, {name: 'login'})(Login);
