import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { withRouter } from 'react-router'
import { AUTH_TOKEN } from 'constants'
import '../styles/NavColumn.css';

class NavColum extends Component{


  render(){
    var curPath = this.props.location.pathname.split("/")[1]
    const authToken = localStorage.getItem(AUTH_TOKEN)
    var links = [
      'Login',
      'Locations',
      'Voucher Codes',
      'Instructors',
      'Members',
      'Sessions',
      'Memberships'
    ]
    var htmlLinks = []
    for (var i = 0; i < links.length; i++) {
      if (!authToken){
        curPath ="Login"
      }
      if (links[i] === curPath){
        htmlLinks[i] = <li key={i.toString()} className="nav-item">
          <Link className="nav-link active" to={links[i].replace(' ', '-')}>{links[i]}</Link>
        </li>
      }
      else{
        htmlLinks[i] = <li key={i.toString()} className="nav-item">
          <Link className="nav-link" to={links[i].replace(' ', '-')}>{links[i]}</Link>
        </li>
      }

    }
    return(
        <ul className="nav nav-pills flex-column">
          {htmlLinks}
        </ul>
    )
  }
}

export default withRouter(NavColum)
