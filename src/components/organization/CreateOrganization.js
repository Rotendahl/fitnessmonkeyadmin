import React, { Component } from 'react'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'

class CreateOrganization extends Component {
  state = {
    logo: "",
    title: ""
  }
  render () {
    return (
      <form onSubmit={(e) => this._createOrganization(e)}>
        <div className="form-group">
          <label htmlFor="title">Title:</label>
          <input
            type="text"
            className="form-control"
            value={this.state.title}
            onChange={e => this.setState({title: e.target.value})}
            id="title"
            placeholder="TræningsHjørnet"
          />
        </div>
        <div className="form-group">
          <label htmlFor="logo">Logo Url</label>
          <input
            type="text"
            className="form-control"
            id="logo"
            placeholder="mig.png"
            value={this.state.logo}
            onChange={e => this.setState({logo: e.target.value})}
          />
        </div>
        <button
          type="submit"
          className="btn btn-primary"
        >
          Opret
        </button>
      </form>
    )
  }

  _createOrganization = async(e) => {
    e.preventDefault()
    const {logo, title} = this.state
    await this.props.createOrg({
      variables : {
        input: {
          logo: logo,
          title: title
        }
      }
    })
    this.props.history.push('/OrganizationGrid')
  }
}


const CREATE_ORGANIZATION = gql`
  mutation CreateOrganization($input: CreateOrganizationInput!){
    createOrganization(input: $input){
      successful
      payload {
        title
        logo
      }
      errors{
        type
        metadata {
          key
          value
        }
        code
      }
    }
  }
`

export default graphql(CREATE_ORGANIZATION, {name: 'createOrg'})(CreateOrganization);
