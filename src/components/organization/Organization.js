import React, { Component } from 'react'
import  '../../styles/Organization.css';

class Organization extends Component {
  render() {
    return (
      <div className="col-4 OrganizationBox text-center">
          <img
            className="img-fluid rounded"
            alt="Organization logo"
            src={this.props.org.logo}
          />
          <h3>{this.props.org.title}</h3>
      </div>
    )
  }
}

export default Organization
