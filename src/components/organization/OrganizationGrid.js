import React, { Component } from 'react'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'
import OrganizationRow from './OrganizationRow'

class OrganizationGrid extends Component {
  _createRows(orgs){
    var rows = [];
    var i = 0;
    while(orgs - i > 2){
      rows.push(orgs[i++])
      rows.push(orgs[i++])
      rows.push(orgs[i++])
    }
    rows.push(orgs.slice(i, orgs.length));
    return rows
  }

  render() {
    if (this.props.orgs && this.props.orgs.loading){
      return <div>Loading</div>
    }
    if (this.props.orgs && this.props.orgs.error){
      return <div>Error</div>
    }
    const rows = this._createRows(this.props.orgs.getOrganization);

    return (
      <div className="container">
        {rows.map( (row, index) => <OrganizationRow key={index} orgs={row}/>)}
      </div>
    )
  }
}

const ORG_QUERY = gql `
  query {
    getOrganization {
      id
      title
      logo
    }
  }
`

export default graphql(ORG_QUERY, { name: 'orgs' }) (OrganizationGrid)
