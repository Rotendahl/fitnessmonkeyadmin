import React, { Component } from 'react'
import Organization from './Organization'

class OrganizationRow extends Component {
  render() {
    const orgs = this.props.orgs
    return (
      <div className="row">
        {orgs.map(org => <Organization key={org.id} org={org}/>)}
      </div>
    )
  }
}

export default OrganizationRow
